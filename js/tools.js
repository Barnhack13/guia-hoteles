$(document).ready(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    
    $('.carousel').carousel({
      interval: 2000
    });
    
    $("#contacto").on("show.bs.modal", function (e){
      console.log("El modal se está mostrando");

      $("#btnContacto").removeClass("btn-outline-dark");
      $("#btnContacto").addClass("btn-warning");
      $("#btnContacto").prop("disabled",true);
    });

    $("#contacto").on("hidden.bs.modal", function (e){
      console.log("El modal está oculto");
      $("#btnContacto").addClass("btn-dark");
      $("#btnContacto").prop("disabled",false);
    });
  });